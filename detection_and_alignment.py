import numpy as np
import cv2
import os
from PIL import Image
import math
from mtcnn.mtcnn import MTCNN


def distance(p1, p2):
  dx = p2[0] - p1[0]
  dy = p2[1] - p1[1]
  return math.sqrt(dx*dx+dy*dy)


def scale_rotate_translate(image, angle, center=None, new_center=None, scale=None, resample=Image.BICUBIC):
    if (scale is None) and (center is None):
        return image.rotate(angle=angle, resample=resample)
    nx, ny = x, y = center
    sx = sy = 1.0
    if new_center:
        (nx, ny) = new_center
    if scale:
        (sx, sy) = (scale, scale)
    cosine = math.cos(angle)
    sine = math.sin(angle)
    a = cosine/sx
    b = sine/sx
    c = x-nx*a-ny*b
    d = -sine/sy
    e = cosine/sy
    f = y-nx*d-ny*e
    return image.transform(image.size, Image.AFFINE, (a, b, c, d, e, f), resample=resample)


def crop_face(image, left_eye, right_eye, offset_percent=(0.3,0.3), dest_size=(224, 224)):
    # calculate offsets in original image
    offset_h = math.floor(float(offset_percent[0])*dest_size[0])
    offset_w = math.floor(float(offset_percent[1])*dest_size[1])

    eye_direction = (right_eye[0] - left_eye[0], right_eye[1] - left_eye[1])

    # Compute rotation angle in radians
    rotation = -math.atan2(float(eye_direction[1]), float(eye_direction[0]))
    # Get the distance between the eyes
    dist = distance(left_eye, right_eye)
    # Compute the reference eye-width
    reference = dest_size[0] - 2.0*offset_h
    # Scale factor
    scale = float(dist)/float(reference)

    # Rotate image around the left eye, drop next line for no rotation
    image = scale_rotate_translate(image, center=left_eye, angle=rotation)

    # Crop the rotated image
    crop_xy = (left_eye[0] - scale*offset_h, left_eye[1] - scale*offset_w)
    crop_size = (dest_size[0]*scale, dest_size[1]*scale)
    image = image.crop((int(crop_xy[0]), int(crop_xy[1]), int(crop_xy[0] + crop_size[0]), int(crop_xy[1] + crop_size[1])))

    # Resize it
    image = image.resize(dest_size, Image.ANTIALIAS)
    return image, rotation, dist, scale


def detect_and_align_faces_in_single_frame(frame_arr, face_detection_model):
    """The function detects faces in a single frame and then aligns them.
    Returns the detected faces, the aligned faces and additional imformation such as face bbox,
    eyes location, rotation angle etc."""
    detection = face_detection_model.detect_faces(frame_arr)
    detected_faces, aligned_faces = [], []
    detected_eyes, detected_bboxes = [], []
    angles, eye_distances, scales = [], [], []
    for bbox in detection:
        try:
            # Get bounding box and eye locations
            x1, y1, width, height = bbox['box']
            x2, y2 = x1 + width, y1 + height
            detected_bboxes.append([x1, y1, width, height])
            right_eye = bbox['keypoints']['right_eye']
            left_eye = bbox['keypoints']['left_eye']
            detected_eyes.append([right_eye, left_eye])
            # Extract the face
            face = frame_arr[y1:y2, x1:x2]
            img = Image.fromarray(face)
            detected_faces.append(img)
            # Align the detected face
            frame_img = Image.fromarray(frame_arr)
            aligned_face, rotation, dist, scale = crop_face(frame_img, left_eye=left_eye, right_eye=right_eye)
            aligned_faces.append(aligned_face)
            angles.append(round(rotation, 2))
            eye_distances.append(round(dist, 2))
            scales.append(round(scale, 2))
        except:
            continue
    return detected_faces, detected_bboxes, detected_eyes, aligned_faces, angles, eye_distances, scales


if __name__ == "__main__":
    frames_path = r'C:\Users\user\Desktop\detection_and_alignment\obama_frames'

    # A directory for the detected faces
    detected_faces_output_path = r'C:\Users\user\Desktop\detection_and_alignment\detected_faces'
    if not os.path.exists(detected_faces_output_path):
        os.makedirs(detected_faces_output_path)

    # A directory for the aligned faces
    aligned_faces_output_path = r'C:\Users\user\Desktop\detection_and_alignment\aligned_faces'
    if not os.path.exists(aligned_faces_output_path):
        os.makedirs(aligned_faces_output_path)

    # A txt file for the process and data details
    details_file = open(r'C:\Users\user\Desktop\detection_and_alignment\detection_and_alignment_details_file.txt', 'w')
    details_file.write('frame' + '\t' + 'bbox_x' + '\t' + 'bbox_y' + '\t' + 'bbox_w' + '\t' + 'bbox_h' +
                       '\t' + 'left_eye_x' + '\t' + 'left_eye_y' + '\t' + 'right_eye_x' + '\t' + 'right_eye_y' +
                       '\t' + 'rot_angle' + '\t' + 'eye_dist' + '\t' + 'scale' + '\n')

    face_detection_model = MTCNN()

    frame_count = 0
    for filename in os.listdir(frames_path):
        if filename.endswith(('jpg', 'jpeg')):
            frame = Image.open(frames_path + '\\' + filename)
            frame_arr = np.array(frame)

            # detect and align faces
            detected_faces, detected_bboxes, detected_eyes, aligned_faces, angles, eye_distances,\
                            scales = detect_and_align_faces_in_single_frame(frame_arr, face_detection_model)

            # write faces to directories and details to file
            for i in range(len(detected_faces)):
                cv_img = cv2.cvtColor(np.array(detected_faces[i]), cv2.COLOR_RGB2BGR)
                cv2.imwrite(detected_faces_output_path + '\\' + filename, cv_img)

                cv_img = cv2.cvtColor(np.array(aligned_faces[i]), cv2.COLOR_RGB2BGR)
                cv2.imwrite(aligned_faces_output_path + '\\' + filename, cv_img)

                details_file.write(filename[:-4] + '\t' + str(detected_bboxes[i][0]) +'\t' + str(detected_bboxes[i][1]) +
                                   '\t' + str(detected_bboxes[i][2]) + '\t' + str(detected_bboxes[i][3]) + '\t' +
                                   str(detected_eyes[i][0][0]) + '\t' + str(detected_eyes[i][0][1]) + '\t' +
                                   str(detected_eyes[i][1][0]) + '\t' + str(detected_eyes[i][1][1]) + '\t' +
                                   str(angles[i]) + '\t' + str(eye_distances[i]) + '\t' + str(scales[i]) + '\n')

                details_file.flush()
        frame_count = frame_count + 1
