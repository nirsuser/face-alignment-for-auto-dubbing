
from PIL import Image
import math


def distance(p1,p2):
  dx = p2[0] - p1[0]
  dy = p2[1] - p1[1]
  return math.sqrt(dx*dx+dy*dy)


def scale_rotate_translate(image, angle, center=None, new_center=None, scale=None, resample=Image.BICUBIC):
	if (scale is None) and (center is None):
		return image.rotate(angle=angle, resample=resample)
	nx, ny = x, y = center
	sx = sy = 1.0
	if new_center:
		(nx, ny) = new_center
	if scale:
		(sx, sy) = (scale, scale)
	cosine = math.cos(angle)
	sine = math.sin(angle)
	a = cosine/sx
	b = sine/sx
	c = x-nx*a-ny*b
	d = -sine/sy
	e = cosine/sy
	f = y-nx*d-ny*e
	return image.transform(image.size, Image.AFFINE, (a, b, c, d, e, f), resample=resample)


def crop_face(image, left_eye, right_eye, offset_precent, dest_size=(224,224)):
	# calculate offsets in original image
	offset_h = math.floor(float(offset_precent[0])*dest_size[0])
	offset_w = math.floor(float(offset_precent[1])*dest_size[1])
	# get the direction
	eye_direction = (left_eye[0] - right_eye[0], left_eye[1] - right_eye[1])
	print(eye_direction)

	# calc rotation angle in radians
	rotation = -math.atan2(float(eye_direction[1]), float(eye_direction[0]))
	# distance between them
	dist = distance(left_eye, right_eye)
	# calculate the reference eye-width
	reference = dest_size[0] - 2.0*offset_h
	# scale factor
	scale = float(dist)/float(reference)

	# rotate image around the left eye, drop next line for no rotation
	image = scale_rotate_translate(image, center=left_eye, angle=rotation)

	# crop the rotated image
	crop_xy = (right_eye[0] - scale*offset_h, right_eye[1] - scale*offset_w)
	crop_size = (dest_size[0]*scale, dest_size[1]*scale)
	image = image.crop((int(crop_xy[0]), int(crop_xy[1]), int(crop_xy[0] + crop_size[0]), int(crop_xy[1] + crop_size[1])))
	# resize it
	image = image.resize(dest_size, Image.ANTIALIAS)
	return image



result_images_path = r'C:\Users\user\Desktop'
image_path = r'C:\Users\user\Desktop\detection_and_alignment\obama_frames\00353.jpg'

image = Image.open(image_path)
left_eye_coords = (591, 196)
right_eye_coords = (480, 208)

res_image = crop_face(image, left_eye=left_eye_coords, right_eye=right_eye_coords, offset_precent=(0.3,0.3))
res_image.save(result_images_path + '\\result_offset_precent_0.3_with_rotation.jpg')



