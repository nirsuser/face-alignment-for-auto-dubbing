# Face alignment for auto dubbing

Performs face detection using MTCNN and spatial alignment.
Preprocess for an auto dubbing model.